<?php
namespace app\modules\admin\controllers;
use app\models\User;
use yii\web\Controller;
use Yii;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    
	
	public function actionUpdateuser($id){	
		$user=User::findOne($id);
		$pass=$user->password;
		if($user->load($_POST)){
			if($_POST['User']['password']==''){
				$user->password=$pass;
			}else{
				$user->password=Yii::$app->security->generatePasswordHash($_POST['User']['password']);
			}
			if($user->save()){
				$this->redirect(array('/admin'));
			}			
		}
		return $this->render('update',array('model'=>$user));
		
	}
	
	public function actionAdduser(){	
		$user=new User();
		if($user->load($_POST)){
			$user->password = Yii::$app->security->generatePasswordHash($_POST['User']['password']);
			if($user->save()){
				$this->redirect(array('/admin'));
			}
		}
		return $this->render('create',array('model'=>$user));
		
		
	}
	
	public function actionDeleteuser($id){		
		$user=User::findOne($id);
		$user->delete();
		$this->redirect(array('/admin'));
	}
}