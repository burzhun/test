<?php
namespace app\modules\admin\controllers;
use yii\web\Controller;
use app\models\User;
use app\models\Class1;

class ClassController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
	
	public function actionUpdate($id){	
		$class=Class1::findOne($id);
		if($class->load($_POST)&& $class->save()){
			$this->redirect(array('/admin'));
		}
		return $this->render('update',array('model'=>$class));
		
	}
	
	public function actionCreate(){	
		$class=new Class1();
		if($class->load($_POST)&& $class->save()){
			$this->redirect(array('/admin'));
		}
		return $this->render('create',array('model'=>$class));
		
		
	}
	
	public function actionDelete($id){		
		$class=Class1::findOne($id);
		$class->delete();
		$this->redirect(array('/admin'));
	}
}