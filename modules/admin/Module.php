<?php

namespace app\modules\admin;

class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();
		if(\Yii::$app->user->getIsGuest()||\Yii::$app->user->identity->role!='admin'){
			\Yii::$app->getResponse()->redirect(array('/'));
		}
        $this->params['foo'] = 'bar';
        // ...  other initialization code ...
    }
}