<?php
 
use app\models\Ingridient;
use app\models\Class1;
use app\models\User;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\Helpers\Html;
 
$dataProvider = new ActiveDataProvider([
    'query' => User::find(),
    'pagination' => [
        'pageSize' => 20,
    ],
]);
$dataProvider2 = new ActiveDataProvider([
    'query' => Class1::find(),
    'pagination' => [
        'pageSize' => 20,
    ],
]);
 ?>
 Пользователи

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'username',
		[
			'class' => 'yii\grid\ActionColumn',
			'template' => '{update} {delete}',
		   'buttons' => [
				'update' => function ($url,$model) {
                        return Html::a(
                                '<span class="glyphicon glyphicon-pencil"></span>', 
                                '/basic/web/admin/updateuser/'.$model->id);
                },
				'delete' => function($url,$model){
					return '<a href="/basic/web/admin/deleteuser/'.$model->id.'" title="Delete" aria-label="Delete" 
					data-confirm="Are you sure you want to delete this user?" data-method="post"
					data-pjax="0"><span class="glyphicon glyphicon-trash"></span>
					</a>';
				}
		   ]
		]
    ],
]); ?>
<a class='btn btn-success' href='/basic/web/admin/adduser'>Добавить пользователя</a>
<br>
<br>
 Классы

<?= GridView::widget([
    'dataProvider' => $dataProvider2,
    'columns' => [
        'id',
        'name',
		[
			'class' => 'yii\grid\ActionColumn',
			'template' => '{update} {delete}',
		   'buttons' => [
				'update' => function ($url,$model) {
                        return Html::a(
                                '<span class="glyphicon glyphicon-pencil"></span>', 
                                '/basic/web/admin/class/update/'.$model->id);
                },
				'delete' => function($url,$model){
					return '<a href="/basic/web/admin/class/delete/'.$model->id.'" title="Delete" aria-label="Delete" 
					data-confirm="Are you sure you want to delete this item?" data-method="post"
					data-pjax="0"><span class="glyphicon glyphicon-trash"></span>
					</a>';
				}
		   ]
		]
    ],
]); ?>
<br>
<a class='btn btn-success' href='/basic/web/admin/class/create'>Добавить</a>
