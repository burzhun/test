<?php
namespace app\modules\teacher\controllers;
use yii\web\Controller;
use app\models\User;
use app\models\Student;
use app\models\Score;
use app\models\Date;
use app\models\Class1;

class ClassController extends Controller
{
    public function actionView($id)
    {
    	$class=Class1::findOne($id);
        return $this->render('view',['model'=>$class]);
    }
	
	public function actionUpdatestudent($id){	
		$student=Student::findOne($id);
		if($student->load($_POST)&& $student->save()){
			$this->redirect(array('/teacher/class/view/'.$student->class_id));
		}
		return $this->render('create',array('model'=>$student));		
	}
	
	public function actionAddstudent($id){	
		$student=new Student();
		if($student->load($_POST)){
			$student->class_id=$id;
			if($student->save())
				$this->redirect(array('/teacher/class/view/'.$id));
		}
		return $this->render('create',array('model'=>$student));
		
		
	}
	
	public function actionDeletestudent($id){		
		$student=Student::findOne($id);
		$class_id=$student->class_id;
		$student->delete();
		$this->redirect(array('/teacher/class/view/'.$class_id));
	}

	public function actionScores($id)
	{
		$dates=Date::find()->where(['class_id'=>$id])->orderBy(['value'=>SORT_ASC])->all();
		$class=Class1::findOne($id);
		return $this->render('scores',array('class_id'=>$id,'dates'=>$dates,'class'=>$class));
	}

	public function actionAdddate($id){
		$date=new Date();
		if($date->load($_POST)){
			$date->class_id=$id;
			if($date->save())
				$this->redirect(array('/teacher/class/scores/'.$id));
		}
		return $this->render('adddate',['model'=>$date]);
	}

	public function actionAddscoreajax()
	{
		$score=new Score();
		$score->student_id=$_POST['student_id'];
		$score->date_id = $_POST['date_id'];
		$score->value=$_POST['value'];
		$score->save();
		echo $score->id;
	}

	public function actionUpdatescoreajax()
	{
		$score=Score::findOne($_POST['score_id']);
		$score->value=$_POST['value'];
		$score->save();
		echo $score->id;
	}
}