<?php

namespace app\modules\teacher;

class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();
		if(\Yii::$app->user->getIsGuest()||\Yii::$app->user->identity->role!='teacher'){
			\Yii::$app->getResponse()->redirect(array('/'));
		}
    }
}