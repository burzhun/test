<?php
 
use app\models\Ingridient;
use app\models\Student;
use app\models\User;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\Helpers\Html;
 
$dataProvider = new ActiveDataProvider([
    'query' => Student::find()->where(['class_id'=>$class_id]),
    'pagination' => [
        'pageSize' => 20,
    ],
]);
 
$columns=[];
$columns[]=[
           'label'=>'',
           'format' => 'raw',
         'value'=>function ($data) {
              return $data->name;
          },
        ];
foreach ($dates as $key => $date) {
    $columns[]=[
           'label'=>$date->value,
           'format' => 'raw',
         'value'=>function ($data) use($date){
              return $data->getScore($date);
          },
        ];
}?>
<div>
  Оценки класса <?=$class->name?><br>
  <a class='btn btn-success' href='/basic/web/teacher/class/adddate/<?=$class->id;?>'>Добавить дату</a>

</div><br>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns
]); ?>

<script type="text/javascript">
window.onload=function(){
  $('.add_score').click(function(){
    var value=prompt('Введите оценку');
    var student_id=$(this).attr('student_id');
    var date_id=$(this).attr('date_id');
    var t=$(this);
    $.post('/basic/web/teacher/class/addscoreajax/',{student_id:student_id,date_id:date_id,value:value},function(data){
      var html='<a href="#" class="update_score" score_id="'+data+'">'+value+'</a>';
      t.parent().html(html);
    });
  });

  $('body').on('click','.update_score',function(){
    var value=prompt('Введите оценку');
    var score_id=$(this).attr('score_id');
    var t=$(this);
    $.post('/basic/web/teacher/class/updatescoreajax/',{score_id:score_id,value:value},function(data){
      t.text(value);
    });
  });
}
</script>

