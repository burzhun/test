<?php

use app\models\Class1;
use app\models\Student;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\Helpers\Html;

$dataProvider = new ActiveDataProvider([
    'query' => Student::find()->where(['class_id'=>$model->id]),
    'pagination' => [
        'pageSize' => 20,
    ],
]);
?>

<div>
	<a href='/basic/web/teacher/'>Список классов</a><br>
	Список учеников в классе <?=$model->name;?><br>
</div>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'name',
		[
			'class' => 'yii\grid\ActionColumn',
			'template' => '{update} {delete}',
		   'buttons' => [
				'update' => function ($url,$model) {
                        return Html::a(
                                '<span class="glyphicon glyphicon-pencil"></span>', 
                                '/basic/web/teacher/class/updatestudent/'.$model->id);
                },
				'delete' => function($url,$model){
					return '<a href="/basic/web/teacher/class/deletestudent/'.$model->id.'" title="Delete" aria-label="Delete" 
					data-confirm="Are you sure you want to delete this student?" data-method="post"
					data-pjax="0"><span class="glyphicon glyphicon-trash"></span>
					</a>';
				}
		   ]
		]
    ],
]); ?>
<a class='btn btn-success' href='/basic/web/teacher/class/addstudent/<?=$model->id;?>'>Добавить</a>
<br>