<?php
 
use app\models\Ingridient;
use app\models\Class1;
use app\models\User;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\Helpers\Html;
 
$dataProvider = new ActiveDataProvider([
    'query' => Class1::find()->where(['user_id'=>\Yii::$app->user->id]),
    'pagination' => [
        'pageSize' => 20,
    ],
]);
 ?>
 Классы

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
           'label'=>'Класс',
           'format' => 'raw',
	       'value'=>function ($data) {
	            return Html::a($data->name,'class/view/'.$data->id);
	        },
	    ],
        [
           'label'=>'Оценки',
           'format' => 'raw',
	       'value'=>function ($data) {
	            return Html::a($data->name,'class/scores/'.$data->id);
	        },
	    ],
    ],
]); ?>

