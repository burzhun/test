<?php

namespace app\models;
use app\models\ProductIngridient;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $name
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

	public function getIngridients()
    {
        return $this->hasMany(Ingridient::className(), ['id' => 'ingridient_id'])
            ->viaTable('product_ingridient', ['product_id' => 'id']);
    }
	
	public function hasIngridient($ingridient_id){
		if(ProductIngridient::find()->where(['ingridient_id' => $ingridient_id,'product_id'=>$this->id])->all()){
			return true;
		}else{
			return false;
		}
	}
	
	
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
