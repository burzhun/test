<?php

namespace app\models;

use Yii;

/**
 *
 * @property integer $id
 * @property string $name
 */
class Score extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'score';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_id','date_id','value'], 'required'],
            ['value', 'in', 'range' => [1, 2, 3,4,5]]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
        ];
    }

}
