<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_ingridient".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $ingridient_id
 */
class ProductIngridient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_ingridient';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'ingridient_id'], 'required'],
            [['product_id', 'ingridient_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'ingridient_id' => 'Ingridient ID',
        ];
    }
}
