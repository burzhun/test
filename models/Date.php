<?php

namespace app\models;

use Yii;

/**
 *
 * @property integer $id
 * @property string $name
 */
class Date extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value','class_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
        ];
    }
}
