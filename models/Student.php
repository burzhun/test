<?php

namespace app\models;

use Yii;

/**
 *
 * @property integer $id
 * @property string $name
 */
class Student extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','class_id'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    public function getScore($date)
    {
        $score=Score::findOne(['date_id'=>$date->id,'student_id'=>$this->id]);
        if($score)
            return '<a href="#" class="update_score" score_id="'.$score->id.'">'.$score->value.'</a>';
        else return '<a href="#" class="add_score" student_id="'.$this->id.'" date_id="'.$date->id.'">Добавить</a>';;
    }
}
