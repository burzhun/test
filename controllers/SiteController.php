<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        //User::Add('user1','teacher1','112358','teacher');
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if(\Yii::$app->user->identity->role=='admin')
                \Yii::$app->getResponse()->redirect(array('/admin/'));
            if(\Yii::$app->user->identity->role=='teacher')
                \Yii::$app->getResponse()->redirect(array('/teacher/'));
            //return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

	public function actionFind(){
		$array1=[];
		foreach($_POST['Ingridients'] as $key=>$value){
			$array1[]=$key;
		}		
		$s=implode(',',$array1);
		$sql="SELECT product.*,count(product_ingridient.id) as count,count(p2.id) as count2 FROM `product` 
		left join product_ingridient on product_ingridient.product_id=product.id and product_ingridient.ingridient_id in ({$s})
			left  join product_ingridient p2 on p2.product_id=product.id and p2.ingridient_id not in ({$s})
			 GROUP BY product.id having count2=0 order by count desc";
		$res=\Yii::$app->getDb()->createCommand($sql)->queryAll();
		if(isset($res[0])){
			foreach($res as $array){
				echo $array['name'].'<br>';
			}
		}else{
			$sql="SELECT product.*,count(product_ingridient.id) as count FROM `product` 
			left join product_ingridient on product_ingridient.product_id=product.id and product_ingridient.ingridient_id in ({$s})			
			 GROUP BY product.id having count>2 order by count desc";
			 $res=\Yii::$app->getDb()->createCommand($sql)->queryAll();
			if(isset($res[0])){
				foreach($res as $array){
					echo $array['name'].'<br>';
				}
			}else{
				echo 'Ничего не найдено';
			}
		}
		
	}
    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
